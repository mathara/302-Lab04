package base;

import java.util.ArrayList;
import java.util.UUID;

public class Carta {
	private UUID id;
	private String nome;
	private int custoMana;
	
	public Carta (UUID id, String nome,int custoMana){
		this.id =  id;
		this.nome = nome;
		this.custoMana = custoMana;
	}
	
	public Carta(String nome, int custoMana){
		this.id =  UUID.randomUUID();
		this.nome = nome;
		this.custoMana = custoMana;
	}
	
	public UUID getId(){
		return id;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public int getCustoMana(){
		return custoMana;
	}
	
	public int setCustoMana(){
		return custoMana;
	}
	
	public void usar(Carta alvo){}
	
	public void usar(ArrayList<Carta> alvos) {}
	
	//@Override
	public String toString(){
			String out = getNome()+ "(ID:" + getId()+")\n";
			out += "CustoMana:" + getCustoMana()+"\n";
			return out;
		}

}
