package base.cartas;

import java.util.UUID;
import base.Carta;

public class Lacaio extends Carta{
	private int ataque;
	private int vidaAtual;
	private int vidaMaxima;
	
	//metodo construtor 
	public Lacaio(UUID id, String nome, int custoMana, int ataque, int vidaAtual, int vidaMax){
		super(id, nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	//construtor reduzido
	public Lacaio(String nome, int custoMana, int ataque, int vidaAtual, int vidaMax){
		super(nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	//Demais metodos
	public int getAtaque(){
		return ataque;
	}
	
	public void setAtaque(int ataque){
		this.ataque = ataque;
	}
	
	public int getVidaAtual(){
		return vidaAtual;
	}
	
	public void setVidaAtual(int vidaAtual){
		this.vidaAtual = vidaAtual;
	}
	
	public int getVidaMaxima(){
		return vidaMaxima;
	}
	
	public void setVidaMaxima(int vidaMaxima){
		this.vidaMaxima = vidaMaxima;
	}
	
	@Override
	//Alterar
	public String toString(){
		String out = super.toString();
		out += "Ataque = " +getAtaque() + "\n";
		out += "Vida Atual = " +getVidaAtual() + "\n";
		out += "Vida Maxima = " +getVidaMaxima() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.vidaAtual -= this.ataque;
		alvo = aux;
	}

}
