package base.cartas.magia;

import java.util.ArrayList;
import java.util.UUID;
import base.Carta;
import base.cartas.Lacaio;

public class DanoArea extends Dano{

	public DanoArea(UUID id,String nome, int custoMana, int dano) {
		super(id, nome, custoMana, dano);
	}
	
	public DanoArea(String nome, int custoMana, int dano) {
		super(nome, custoMana, dano);
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		return out;
	}
	
	@Override
	public void usar(ArrayList<Carta> alvos){
		int j = alvos.size();
		int i = 0;
		
		while (i < j){	
		Lacaio aux = (Lacaio) alvos.get(i);
		aux.setVidaAtual(aux.getVidaAtual() - this.getDano());
		alvos.add(i,aux);
		alvos.remove(i+1);
		i++;
		}
		
	}
	

}
