package base.cartas.magia;

import java.util.UUID;
import base.Carta;
import base.cartas.Lacaio;

public class Buff extends Magia{
	private int aumentoEmAtaque;
	private int aumentoEmVida;
	
	public Buff(UUID id, String nome, int custoMana, int aumentoEmAtaque, int aumentoEmVida) {
		super(id, nome, custoMana);
		this.aumentoEmAtaque = aumentoEmAtaque;
		this.aumentoEmVida = aumentoEmVida;
	}

	public Buff(String nome, int custoMana, int aumentoEmAtaque, int aumentoEmVida) {
		super(nome, custoMana);
		this.aumentoEmAtaque = aumentoEmAtaque;
		this.aumentoEmVida = aumentoEmVida;
	}
	
	public int getAumentoEmAtaque(){
		return aumentoEmAtaque;
	}
	
	public void setAumentoEmAtaque(int aumentoEmAtaque){
		this.aumentoEmAtaque = aumentoEmAtaque;
	}
	
	public int getAumentoEmVida(){
		return aumentoEmVida;
	}
	
	public void setAumentoEmVida(int aumentoEmVida){
		this.aumentoEmVida = aumentoEmVida;
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		out += "Aumento em Ataque = " +getAumentoEmAtaque() + "\n";
		out += "Aumento em Vida = " +getAumentoEmVida() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.setAtaque(aux.getAtaque() + this.aumentoEmAtaque );
		aux.setVidaAtual(aux.getVidaAtual() + this.aumentoEmVida);
		
		//não foi especificado, se poderia ou não fazer isso
		if (aux.getVidaAtual() == aux.getVidaMaxima()){
			aux.setVidaMaxima(aux.getVidaMaxima() + this.aumentoEmVida);
		}
		
		alvo = aux;
	}
	
}
