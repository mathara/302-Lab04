package base;

import java.util.ArrayList;
import java.util.Collections;
import util.*;


public class Baralho {
	private ArrayList<Carta> vetorCartas;
	
	public Baralho(){
		vetorCartas = new ArrayList<Carta>();
	}
	
	public void adicionarCarta (Carta card){
		if (vetorCartas.size() <  Util.MAX_CARDS){
			vetorCartas.add(card);
			// não preciso de uma váriavel que contenha o tamanho
		}
	}
	
	public Carta comprarCarta(){
		Carta aux = vetorCartas.get(vetorCartas.size()-1);	//pega o lacaio do topo da pilha
		vetorCartas.remove(vetorCartas.size()-1);					//remove da pilha
		return aux;													//Lacaio do topo da pilha
	}
	
	public void embaralhar(){
		Collections.shuffle(vetorCartas);
		ArrayList<Carta> baux = vetorCartas;
		Collections.reverse(baux);
		System.out.println(baux);
	}
	
	public void imprimirBaralho(){
		System.out.println(vetorCartas);
	}
}
