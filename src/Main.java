import java.util.ArrayList;

import base.*;
import base.cartas.*;
import base.cartas.magia.*;

public class Main {
	public static void main(String[] args) {
		//instanciando objetos
		//lacaio
	Carta card1 = new Lacaio("Frodo Bolseiro",2,1,1,1);
	Carta card2 = new Lacaio("Aragorn", 5, 7, 6,6);
	Carta card3 = new Lacaio("Legolas", 8, 4, 6,6);
	Carta card4 = new Lacaio("Druida da Noite", 7, 8, 6,6);
	Carta card5 = new Lacaio("Cientista Maluco", 3, 5, 4,4);
	Carta card6 = new Lacaio("Jacicoê , Marty", 6, 4, 5,5);
	Carta card7 = new Lacaio("Leroy Jekins", 6, 2, 5,5);
	Carta card8 = new Lacaio("Pirata sem tesouro", 4, 5, 4,4);
	Carta card9 = new Lacaio("Dragão bebê", 4, 3, 3,3);
	Carta card  = new Lacaio("Mago Negro", 10, 7, 10,10);
	Carta cardx  = new Lacaio("Logan", 10, 10, 10,10);
	
	
		//magia
	Carta mag1 = new DanoArea("You shall not pass", 4, 7);
	Carta mag2 = new DanoArea("Telecinese", 3, 2);
	Carta mag8 = new Dano("Remove Bender", 6, 7);
	Carta mag9 = new Dano("Poison", 3, 5);
	Carta mag = new Dano("Snooze", 2, 4);
	
	Carta mag3 = new Buff("Katon", 5, 4, 5);
	Carta mag4 = new Buff("Suiton", 5, 4, 5);
	Carta mag5 = new Buff("Fuuton", 5, 4, 5);
	Carta mag6 = new Buff("Raiton", 5, 4, 5);
	Carta mag7 = new Buff("Doton", 5, 4, 5);
	
	
	//Baralho
	Baralho deck = new Baralho();
	ArrayList<Carta> deck2 = new ArrayList<Carta>();
	
	//colocando cartas no deck
	deck.adicionarCarta(card1);
	deck.adicionarCarta(card2);
	deck.adicionarCarta(card3);
	deck.adicionarCarta(card4);
	deck.adicionarCarta(card5);
	deck.adicionarCarta(card6);
	deck.adicionarCarta(card7);
	deck.adicionarCarta(card8);
	deck.adicionarCarta(card9);
	deck.adicionarCarta(cardx);
	deck.adicionarCarta(card);
	
	deck.adicionarCarta(mag1);
	deck.adicionarCarta(mag2);
	deck.adicionarCarta(mag3);
	deck.adicionarCarta(mag4);
	deck.adicionarCarta(mag5);
	deck.adicionarCarta(mag6);
	deck.adicionarCarta(mag7);
	deck.adicionarCarta(mag8);
	deck.adicionarCarta(mag9);
	deck.adicionarCarta(mag);
	
	deck2.add(card1);
	deck2.add(card2);
	deck2.add(card3);
	
	deck.embaralhar();
	deck.imprimirBaralho();
	
	//teste dos métodos usar
	System.out.println(cardx);
	
	card9.usar(cardx);//usando lacaio
	
	System.out.println(cardx);
	
	mag9.usar(cardx);//usando Dano
	
	System.out.println(cardx);
	
	mag3.usar(cardx);//usando Buff
	
	System.out.println(cardx);
	System.out.println(deck2.size());
	mag1.usar(deck2);
	for (int i = 0; i < deck2.size(); i++) {
		System.out.println(deck2.get(i));
	}
	
	
	
	}
}
